var http = require('http');
var MongoClient = require('mongodb').MongoClient;

var handleRequest = function(request, response) {
  console.log('Received request for URL: ' + request.url);
 
// Connection URL
var url = 'mongodb://mongodb:27017/test1';
// Use connect method to connect to the Server
MongoClient.connect(url, function(err, db) {
  console.log("Connected correctly to server");
  var collection = db.collection('test');
  collection.find({}).toArray(function(err, docs) {
    console.log("Found the following records");
    response.writeHead(200);
    docs.forEach(function(doc) {
        response.write(JSON.stringify(doc) + '\n');
    });
    response.end('Completed (on ' + process.env.APP_NODE + ')!');
  });
 
  db.close();
});

};

var www = http.createServer(handleRequest);
www.listen(80);
